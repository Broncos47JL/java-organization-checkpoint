package com.galvanize.formatters;

import com.galvanize.Booking;

public class JSONFormatter implements Formatter{
    @Override
    public String format(Booking book){
        String format;
        String roomType = book.getRoomType().getTypeRoom();
        format = "{\n" +
                "  \"type\": \"" + roomType + "\",\n" +
                 "  \"roomNumber\": " + book.getRoomNumber() + ",\n" +
                 "  \"startTime\": \"" + book.getStartTime() + "\",\n" +
                 "  \"endTime\": \"" + book.getEndTime() + "\"\n" +
                 "}";
        return format;
    }
}
