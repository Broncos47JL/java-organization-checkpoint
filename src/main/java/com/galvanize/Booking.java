package com.galvanize;

public class Booking {

    public enum RoomType {
        CONFERENCE_ROOM("Conference Room"),
        SUITE("Suite"),
        AUDITORIUM("Auditorium"),
        CLASSROOM("Classroom");

        private String typeRoom;

        RoomType(String typeRoom){
            this.typeRoom = typeRoom;
        }

        public String getTypeRoom(){
            return typeRoom;
        }
    }

    private RoomType roomType;
    private String roomNumber;
    private String startTime;
    private String endTime;

    public Booking(RoomType roomType, String roomNumber, String startTime, String endTime) {
        this.roomType = roomType;
        this.roomNumber = roomNumber;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public static Booking parse(String booking) {
        RoomType type = null;
        String room;
        String start;
        String end;

        Booking myBooking;

        switch (booking.charAt(0)){
            case 'r': type = RoomType.CONFERENCE_ROOM;
            break;
            case 's': type = RoomType.SUITE;
            break;
            case 'a': type = RoomType.AUDITORIUM;
            break;
            case 'c': type = RoomType.CLASSROOM;
        }

        room = booking.substring(1,(booking.indexOf('-')));

        start = booking.substring((booking.indexOf('-')+1),(booking.lastIndexOf('-')));

        end = booking.substring((booking.lastIndexOf('-')+1),(booking.length()));

        myBooking = new Booking(type, room, start, end);

        return myBooking;
    }

    public RoomType getRoomType() {
        return roomType;
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getEndTime() {
        return endTime;
    }
}
