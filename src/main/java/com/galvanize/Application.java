package com.galvanize;

import com.galvanize.formatters.*;

public class Application {

    public static void main(String[] args) {
        Formatter myFormat = getFormatter(args[1]);
        Booking myBooking = Booking.parse(args[0]);
        System.out.print(myFormat.format(myBooking));
    }

    public static Formatter getFormatter(String format){
        Formatter formatter = null;
        switch (format){
            case "html": formatter =new HTMLFormatter();
            break;
            case "json": formatter = new JSONFormatter();
            break;
            case "csv": formatter = new CSVFormatter();
        }
        return formatter;
    }
}