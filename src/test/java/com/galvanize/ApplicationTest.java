package com.galvanize;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ApplicationTest {

    PrintStream original;
    ByteArrayOutputStream outContent;

    // This block captures everything written to System.out
    @BeforeEach
    public void setOut() {
        original = System.out;
        outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
    }

    // This block resets System.out to whatever it was before
    @AfterEach
    public void restoreOut() {
        System.setOut(original);
    }

    @Test
    public void aTest() {
        // Write your tests here
        // outContent.toString() will give you what your code printed to System.out
    }

    @Test
    public void applicationShouldReceiveTwoArgsToBookConferenceRoomHTML(){
        Application.main(new String[]{"r111-08:30am-11:00am", "html"});
        String result = outContent.toString();
        assertEquals("<dl>\n" +
                "  <dt>Type</dt><dd>Conference Room</dd>\n" +
                "  <dt>Room Number</dt><dd>111</dd>\n" +
                "  <dt>Start Time</dt><dd>08:30am</dd>\n" +
                "  <dt>End Time</dt><dd>11:00am</dd>\n" +
                "</dl>", result);
    }

    @Test
    public void applicationShouldReceiveTwoArgsToBookConferenceRoomCSV(){
        Application.main(new String[]{"a111-08:30am-11:00am", "csv"});
        String result = outContent.toString();
        assertEquals("type,room number,start time,end time\n" +
                "Auditorium,111,08:30am,11:00am", result);
    }

    @Test
    public void applicationShouldReceiveTwoArgsToBookConferenceRoomJSON(){
        Application.main(new String[]{"s111-08:30am-11:00am", "json"});
        String result = outContent.toString();
        assertEquals("{\n" +
                "  \"type\": \"Suite\",\n" +
                "  \"roomNumber\": 111,\n" +
                "  \"startTime\": \"08:30am\",\n" +
                "  \"endTime\": \"11:00am\"\n" +
                "}", result);
    }

    @Test
    public void applicationShouldReceiveTwoArgsToBookConferenceRoomUnoMas(){
        Application.main(new String[]{"c111-08:30am-11:00am", "csv"});
        String result = outContent.toString();
        assertEquals("type,room number,start time,end time\n" +
                "Classroom,111,08:30am,11:00am", result);
    }
}