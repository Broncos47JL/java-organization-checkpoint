package com.galvanize.formatters;

import com.galvanize.Booking;

public class HTMLFormatter implements Formatter{
    @Override
    public String format(Booking book){
        String format;
        String roomType = book.getRoomType().getTypeRoom();
        format = "<dl>\n" +
                "  <dt>Type</dt><dd>" + roomType + "</dd>\n" +
                "  <dt>Room Number</dt><dd>" + book.getRoomNumber() + "</dd>\n" +
                "  <dt>Start Time</dt><dd>" + book.getStartTime() + "</dd>\n" +
                "  <dt>End Time</dt><dd>"+ book.getEndTime() + "</dd>\n" +
                "</dl>";
        return format;
    }
}
