package com.galvanize.formatters;

import com.galvanize.Booking;

public class CSVFormatter implements Formatter{
    @Override
    public String format(Booking book){
        String format;
        String roomType = book.getRoomType().getTypeRoom();
        format = "type,room number,start time,end time\n" +
                 "" + roomType + "," + book.getRoomNumber() + "," + book.getStartTime() + "," + book.getEndTime();
        return format;
    }
}
